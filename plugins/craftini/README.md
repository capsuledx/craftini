# Craftini plugin for Craft CMS

This is the special flavor to the Craftini registration.

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Craftini, follow these steps:

1. Download & unzip the file and place the `craftini` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3. Install plugin in the Craft Control Panel under Settings > Plugins
4. The plugin folder should be named `craftini` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Craftini works on Craft 2.4.x and Craft 2.5.x.

## Craftini Overview

-Insert text here-

## Configuring Craftini

-Insert text here-

## Using Craftini

-Insert text here-

## Craftini Roadmap

Some things to do, and ideas for potential features:

* Release it

## Craftini Changelog

### 1.0.0 -- 2016.06.05

* Initial release

Brought to you by [Craftini Conference](https://craftini.net)