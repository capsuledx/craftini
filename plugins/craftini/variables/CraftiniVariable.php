<?php
namespace Craft;

/**
 * Craftini Variable
 *
 * @author    Craftini Conference
 * @copyright Copyright (c) 2016 Craftini Conference
 * @link      https://craftini.net
 * @package   Craftini
 * @since     1.0.0
 */
class CraftiniVariable
{
	public function getErrors()
	{
		$errors = craft()->httpSession->get('craftini.errors', array());
		craft()->httpSession->remove('craftini.errors');
		return $errors;
	}
	
	public function getOptions()
	{
		$errors = craft()->httpSession->get('craftini.options', array());
		craft()->httpSession->remove('craftini.options');
		return $errors;
	}
}