<?php
/**
 * Craftini plugin for Craft CMS
 *
 * Craftini Translation
 *
 * @author    Craftini Conference
 * @copyright Copyright (c) 2016 Craftini Conference
 * @link      https://craftini.net
 * @package   Craftini
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
