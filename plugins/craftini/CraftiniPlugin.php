<?php
namespace Craft;

class CraftiniPlugin extends BasePlugin
{
	public function init()
	{
		craft()->on('commerce_lineItems.onBeforeSaveLineItem', function($event)
		{
			craft()->craftini->validateOptions($event);
		});
		
		craft()->on('commerce_orders.onOrderComplete', function($event)
		{
			craft()->craftini->registerNewAttendee($event);
		});
	}

	public function getName()
	{
		 return Craft::t('Craftini');
	}

	public function getDescription()
	{
		return Craft::t('This is the special flavor to the Craftini registration.');
	}

	public function getDocumentationUrl()
	{
		return '???';
	}

	public function getReleaseFeedUrl()
	{
		return '???';
	}

	public function getVersion()
	{
		return '1.0.0';
	}

	public function getSchemaVersion()
	{
		return '1.0.0';
	}

	public function getDeveloper()
	{
		return 'Craftini Conference';
	}

	public function getDeveloperUrl()
	{
		return 'https://craftini.net';
	}

	public function hasCpSection()
	{
		return false;
	}
}