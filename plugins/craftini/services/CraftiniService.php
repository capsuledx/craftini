<?php
/**
 * Craftini plugin for Craft CMS
 *
 * Craftini Service
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Craftini Conference
 * @copyright Copyright (c) 2016 Craftini Conference
 * @link      https://craftini.net
 * @package   Craftini
 * @since     1.0.0
 */

namespace Craft;

class CraftiniService extends BaseApplicationComponent
{
	public function validateOptions($event)
	{
		$errors = array();
		// Get our Line Item
		$item = $event->params['lineItem'];
		// Check if this is a single or group ticket
		if(isset($item->options['attendeeNames']))
		{
			// Group Ticket
			// Build the lines into a hash
			$attendees = array_combine(explode("\n",$item->options['attendeeNames']), explode("\n",$item->options['attendeeEmails']));
			// Check each hash, up to four elements
			$i = 1;
			if(empty($attendees))
			{
				$errors['attendeeCount'] = 'There is missing info for an attendee.';
			}
			else
			{
				foreach (array_slice($attendees, 0, 4) as $name => $email)
				{
					if($i == 1) { $place = 'First'; }
					if($i == 2) { $place = 'Second'; }
					if($i == 3) { $place = 'Third'; }
					if($i == 4) { $place = 'Fourth'; }
					
					// Check that the name is filled in.
					if( empty(trim($name)) )
					{
						// Uh oh! The name is missing.
						$errors['attendeeName'.$place] = $place.' attendee name is missing.';
					}
					// Check that the email is an email.
					if(!filter_var(trim($email),FILTER_VALIDATE_EMAIL))
					{
						// Uh oh! That's not an email address.
						$errors['attendeeEmail'.$place] = $place.' attendee email is not valid.';
					}
					$i++;
				}
			}
		}
		else
		{
			// Single Ticket
			// Check that the name is filled in.
			if(empty($item->options['attendeeName']))
			{
				// Uh oh! The name is missing.
				$errors['attendeeName'] = 'Attendee name is missing.';
			}
			// Check that the email is an email.
			if(!filter_var($item->options['attendeeEmail'],FILTER_VALIDATE_EMAIL))
			{
				// Uh oh! That's not an email address.
				$errors['attendeeEmail'] = $item->options['attendeeEmail'].' is not a valid email.';
			}
		}
		
		if(!empty($errors))
		{
			craft()->httpSession->add('craftini.errors', $errors);
			craft()->httpSession->add('craftini.options', $item->options);
			$event->performAction = false;
			return false;
		}
		
		return true;
	}
	
	public function registerNewAttendee($event)
	{
		$order = $event->params['order'];
		if ($order->orderStatusId == 1)
		{
			foreach ($order->getLineItems() as  $lineItem)
			{
				if(isset($lineItem->options['attendeeNames']))
				{
					$attendees = array_combine(explode("\n",$lineItem->options['attendeeNames']), explode("\n",$lineItem->options['attendeeEmails']));
					foreach (array_slice($attendees, 0, 4) as $name => $email)
					{
						$name = explode(" ", trim($name), 2);
						$fname = isset($name[0]) && !empty($name[0]) ? $name[0] : 'No';
						$lname = isset($name[1]) && !empty($name[1]) ? $name[1] : 'Name';
					
						$this->addToMailchimp($fname, $lname, $email);
						$this->addToMattermost($fname, $lname, $email);
						$this->addToZoom($fname, $lname, $email);
					}
				}
				else
				{
					$name = explode(" ", trim($lineItem->options['attendeeName']), 2);
					$fname = isset($name[0]) && !empty($name[0]) ? $name[0] : 'No';
					$lname = isset($name[1]) && !empty($name[1]) ? $name[1] : 'Name';
					
					$this->addToMailchimp($fname, $lname, $lineItem->options['attendeeEmail']);
					$this->addToMattermost($fname, $lname, $lineItem->options['attendeeEmail']);
					$this->addToZoom($fname, $lname, $lineItem->options['attendeeEmail']);
				}
			}
		}
	}
	
	public function createUser($fname, $lname, $email)
	{
		// TODO: I might just do this manually for now.
	}
	
	public function addToMailchimp($fname, $lname, $email)
	{
		$client = new \Guzzle\Http\Client('https://us13.api.mailchimp.com/3.0/', [
			'request.options' => [
				'headers' => ['Content-type' => 'application/json'],
				'auth' => [
					'craftini',
					'a70638d7522ede7322eadc01bd5b72c6-us13',
					'Basic'
				],
				'exceptions' => false
			]
		]);
		
		$email_hash = md5(strtolower($email));
		$data = [
			'email_address' => strtolower($email),
			'status' => 'subscribed'
		];
		
		$response = $client->get('lists/efecdb388b/members/'.$email_hash)->send();
		if((string)$response->getStatusCode() == '404')
		{
			$response = $client->post('lists/efecdb388b/members', [], json_encode($data))->send();
		
			if ($response->isSuccessful())
			{
				CraftiniPlugin::log('Mailchimp Sign Up Complete.', LogLevel::Info, true);
				return true;
			}
			
			CraftiniPlugin::log('Mailchimp Request Failed.', LogLevel::Error, true);
			return false;
		}
	}
	
	public function addToMattermost($fname, $lname, $email)
	{
		$client = new \Guzzle\Http\Client('https://chat.craftini.net/api/v3/');
		
		$login = [
			'name' => 'craftini-2016',
			'login_id' => 'info@craftini.net',
			'password' => '3XCEqNfpOJKt'
		];
		
		$response = $client->post('users/login', [], json_encode($login))->send();
		
		if ($response->isSuccessful() && $response->hasHeader('Token'))
		{
			$token = $response->getHeader('Token');
			$data = [
				'invites' => [[
					'email' => $email,
					'firstName' => $fname,
					'lastName' => $lname
				]]
			];
			$response = $client->post('teams/3um58ygutinc9rkh7q8ccpb6jw/invite_members', ['Authorization' => 'BEARER '.$token], json_encode($data))->send();
			if ($response->isSuccessful())
			{
				CraftiniPlugin::log('Mattermost User Invited.', LogLevel::Info, true);
				return true;
			}
			
			CraftiniPlugin::log('Mattermost User Invite Failed.', LogLevel::Error, true);
			return false;
		}
		else
		{
			CraftiniPlugin::log('Mattermost Login Request Failed.', LogLevel::Error, true);
			return false;
		}
	}
	
	public function addToZoom($fname, $lname, $email)
	{
		$client = new \Guzzle\Http\Client('https://api.zoom.us/v1/', [
			'request.options' => ['headers' => ['Content-type' => 'application/json']]
		]);
		
		$data = [
			'api_key' => 'P7T9a4kWR5ewWr_y97KVHQ',
			'api_secret' => 'W9QtC2R6U5SdPCKo6E1L34RXGOQZ0GLAXQEW',
			'data_type' => 'JSON',
			'id' => '203-954-182',
			'email' => $email,
			'first_name' => $fname,
			'last_name' => $lname
		];
		
		$response = $client->post('webinar/register', [], $data)->send();
		
		if ($response->isSuccessful())
		{
			$result = $response->json();
			if( isset($result['error']) )
			{
				CraftiniPlugin::log('Zoom Registration Failed.', LogLevel::Error, true);
				return false;
			}
			CraftiniPlugin::log('Zoom Registration Complete.', LogLevel::Info, true);
			return true;
		}
		CraftiniPlugin::log('Zoom Request Failed.', LogLevel::Error, true);
		return false;
	}
}