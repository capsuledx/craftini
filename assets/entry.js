require('file?name=/../templates/[name].css!postcss!sass!./critical.scss');
//require('style!css!postcss!sass!./critical.scss');

WebFontConfig = {google: { families: [ 'Source+Serif+Pro:400,700:latin' ] }};
(function() {
	var wf = document.createElement('script');
	wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})();

var $ = require('jquery');
$(function() {	
	// Update our states field if a country is selected
	$('select.address-country').on('change', function ()
	{
		// get the value of the selected country.
		var cid = $(this).val();
		var $states = $(this).closest('.address.column').find('select.address-stateId');
		var $stateName = $(this).closest('.address.column').find('input.address-stateName');
		$states.find('option').remove();
		
		$.getJSON("/register/states.json", { country: cid }).done(function(data) {
			if (data.length === 0) {
				// hide the states dropdown, since this country has none.
				$states.addClass('hidden');
				$states.removeAttr('name');

				// show the stateName
				$stateName.removeClass('hidden');
				$stateName.attr('name', $stateName.data('modelname')+'[stateValue]');
			} else {
				// We have states for this country, show the states drop down.
				$states.removeClass('hidden');
				$states.attr('name', $states.data('modelname')+'[stateValue]');

				// We have states for this country, hide the stateName input.
				$stateName.removeAttr('name');
				$stateName.addClass('hidden');
				$stateName.val('');

				// Add all states as options to drop down.
				for (var id in data)
				{
					var state = data[id];
					var $option = $('<option/>');
					$option.attr('value', id).text(state);
					$states.append($option);
				}
			}
		});
	});
		
	// Auto Display the form underneath the ticket option.
	$('.ticket.form .choose.buttons .lucky.button').on('click', function() {
		var form = $(this).parents('.ticket.form');
		var options = form.children('.options');
		var buttons = $(this).parent();
		
		options.removeClass('hidden');
		buttons.addClass('hidden');
	});
});