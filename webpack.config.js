var webpack = require('webpack');
var path = require('path');
var autoprefixer = require('autoprefixer');

module.exports = {
	resolve: {
		modulesDirectories: ['assets/js', 'node_modules']
	},
	entry: ["./assets/entry.js"],
	output: {
		path: "./public",
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{ test: /\.(jpg|gif|png)$/, loader: "file-loader?name=/[path][name].[ext]" },
			{ test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff&name=/assets/fonts/[name].[ext]" },
			{ test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=/assets/fonts/[name].[ext]" }
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		})
	],
	sassLoader: {
		includePaths: [path.resolve(__dirname, "/Users/bryanredeagle/Git/Maxwell/lucky")],
		outputStyle: 'compressed'
 	},
	postcss: function () {
		return [autoprefixer];
	}
};
